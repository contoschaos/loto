import pickle
import csv
import matplotlib.pyplot as plt
import statistics


def savePlot(list, fileName, show):
    fig, ax = plt.subplots()
    ax.plot(list, '.', markersize=1)
    # Save the figure as an image
    fig.savefig('image/{}'.format(fileName), dpi=2000)
    if (show):
        plt.show()


def saveCsv(list, fileName):
    with open('csv/{}'.format(fileName), 'w', newline='') as file:
        writer = csv.writer(file)
        writer.writerows(list)


def processData(dataArray, filePrefix):
    numbersValue = [combinationData[1]
                    for combinationData in dataArray]
    pairsValue = [combinationData[2]
                  for combinationData in dataArray]
    tripletsValue = [combinationData[3]
                     for combinationData in dataArray]

    # print(statistics.median(numbersValue))

    savePlot(sorted(numbersValue), filePrefix+'numbers.png', False)
    savePlot(sorted(pairsValue), filePrefix+'pairs.png', False)
    savePlot(sorted(tripletsValue), filePrefix+'triplets.png', False)

    saveCsv(dataArray, 'unsorted.csv')
    saveCsv(sorted(dataArray, key=lambda x: x[1]), filePrefix+'byNumber.csv')
    saveCsv(sorted(dataArray, key=lambda x: x[2]), filePrefix+'byPair.csv')
    saveCsv(sorted(dataArray, key=lambda x: x[3]), filePrefix+'byTriplet.csv')
