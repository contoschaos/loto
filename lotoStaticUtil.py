from itertools import combinations
import numpy as np
import os

# In case file was not specified, generates given number of draws
TO_GENERATE = 3000
# How many numbers are pulled in one draw
NUMBERS_PULLED = 7
# How many numbers are guessed
NUMBERS_GUESSED = 6
# Upper numerical bound (1 -> number_max) without any offset (49 means 49)
NUMBER_MAX = 49


def rand(n):
    a = np.fromstring(os.urandom(n*4), dtype=np.uint32) >> 5
    b = np.fromstring(os.urandom(n*4), dtype=np.uint32) >> 6
    return (a * 67108864.0 + b) / 9007199254740992.0


def getAllPossible(isTest):
    # Create a list of all elements to choose from
    elements = list(range(1, elementsRange(isTest)))
    # Generate all combinations of k elements without repetition
    return list(combinations(elements, NUMBERS_GUESSED))


def elementsRange(isTest):
    if isTest:
        return NUMBERS_GUESSED + 2
    else:
        return NUMBER_MAX + 1
