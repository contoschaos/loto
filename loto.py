#!/usr/bin/env python
from multiprocessing.pool import Pool
import multiprocessing as mp
import time
import check
import sys
import pickle
import time
import datetime

import lotoStaticUtil as st
import lotoProcessUtil as pr

# System specific
now = datetime.datetime.now()
estimate = now + datetime.timedelta(seconds=5300)
print("Started:\t", '{:%Y-%m-%d %H:%M:%S}'.format(now), "\nEnd approx.:\t",
      '{:%Y-%m-%d %H:%M:%S}'.format(estimate))

# Get the number of available CPU cores
num_threads = mp.cpu_count()
print('Run on threads: ', num_threads)

# Data choosing
fileName = "loto1"
isTest = False
if len(sys.argv) > 1:
    if sys.argv[1].lower() == 'test':
        fileName = 'lotoTest'
        isTest = True
    else:
        fileName = sys.argv[1]

### Process data ###

print("Data processing start.")
start_time = time.time()

pairTuples = []
tripletsTuples = []

pr.setNumbers(fileName + ".csv")

for i, row in enumerate(pr.pairs):
    for j, number in enumerate(row):
        if (i < j):
            key = set([i+1, j+1])
            pairTuples.append((key, number))

for i, area in enumerate(pr.triplets):
    for j, row in enumerate(area):
        for k, number in enumerate(row):
            # to remove/skip unnecessary data
            if i >= j or j >= k or i >= k:
                continue

            key = set([i+1, j+1, k+1])
            tripletsTuples.append((key, number))
sortedTriplets = sorted(tripletsTuples, key=lambda item: item[1])

end_time = time.time()
print('Done processing:', end_time - start_time, 'seconds')

### Parallel evaluation part ###


def evaluateCombinationPrev(combination):
    # remove set() in pairs and triplets
    sumNumb = 0
    sumPair = 0
    sumTriple = 0
    for number in combination:
        sumNumb += pr.numbers[number-1]

    for pairTuple in pairTuples:
        if all(digit in combination for digit in pairTuple[0]):
            sumPair += pairTuple[1]

    for tripletsTuple in tripletsTuples:
        if all(digit in combination for digit in tripletsTuple[0]):
            sumTriple += tripletsTuple[1]
    return (combination, sumNumb, sumPair, sumTriple)


def evaluateCombination(combination):
    sumNumb = 0
    sumPair = 0
    sumTriple = 0
    for number in combination:
        sumNumb += pr.numbers[number-1]

    for pairTuple in pairTuples:
        if combination & pairTuple[0]:  # <-- not sure if correct
            sumPair += pairTuple[1]

    for tripletsTuple in tripletsTuples:  # <-- not sure if correct
        if combination & tripletsTuple[0]:
            sumTriple += tripletsTuple[1]
    return (combination, sumNumb, sumPair, sumTriple)


results = []


def collect_result(result):
    global results
    results.append(result)


def parallelProcess(save, isTest):
    start_time = time.time()
    allPossibleTuples = st.getAllPossible(isTest)
    allPossible = [set(item) for item in allPossibleTuples]

    results = [None] * len(allPossible)
    mid_time = time.time()
    print('Combinations generated in:', mid_time - start_time, 'seconds')
    # protect the entry point
    if __name__ == '__main__':
        # create and configure the process pool
        pool = Pool()
        # issue tasks to the process pool
        for combination in allPossible:
            pool.apply_async(evaluateCombination, args=(
                combination,), callback=collect_result)
        # close the process pool
        pool.close()
        # wait for all tasks to finish
        pool.join()

        if save:
            with open('raw/data_file.pkl', 'wb') as file:
                pickle.dump(results, file)

    end_time = time.time()
    print('Parallel process done in:', end_time - mid_time, 'seconds')


parallelProcess(False, isTest)

start_time = time.time()
check.processData(results, fileName + '_')
end_time = time.time()
print('Done in:', end_time - start_time, 'seconds')
