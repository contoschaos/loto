#!/usr/bin/env python
from multiprocessing.pool import Pool
from itertools import combinations
import csv
import math
import random
import plotly.express as px
import plotly.graph_objects as go
import numpy as np
import time
import pickle
import check

import os
num_threads = os.cpu_count()  # Get the number of available CPU cores
print('Run on threads: ', num_threads)


# In case file was not specified, generates given number of draws
TO_GENERATE = 3000
# How many numbers are pulled in one draw
NUMBERS_PULLED = 5
# How many numbers are guessed
NUMBERS_GUESSED = 5
# Upper numerical bound (1 -> number_max) without any offset (50 means 50)
NUMBER_MAX = 50

# Index/position is specifying the number (number[0] means number 1, number[26] means 27 ...)
# Count of times each Number was drawn
numbers = [0] * NUMBER_MAX

# Dimension are next numbers
# Count of times each number pair was drawn, orderless
pairs = [[0]*NUMBER_MAX for i in range(NUMBER_MAX+1)]
# Count of times each number triplet was drawn, orderless
triplets = [[[0]*NUMBER_MAX for i in range(NUMBER_MAX)]
            for j in range(NUMBER_MAX)]


def addToGroups(pulled, offset):
    copy = np.array(pulled).tolist()  # all pulled
    # Number loop
    for _ in range(NUMBERS_PULLED):
        x = int(copy.pop(0))
        xi = int(x) - offset
        secondCp = np.array(copy).tolist()
        # Pair loop
        for y in copy:
            yi = int(y) - offset

            sortedKeys = sorted([xi, yi])

            # removes  order for pairs
            pairs[sortedKeys[0]][sortedKeys[1]] += 1

            # Each pair is part of triplet
            secondCp.pop(0)  # Copy without Y
            # Triplet loop
            for z in secondCp:
                zi = int(z) - offset
                # Removes  order for triplets
                sortedKeys = sorted([xi, yi, zi])
                triplets[sortedKeys[0]][sortedKeys[1]][sortedKeys[2]] += 1


def rand(n):
    a = np.fromstring(os.urandom(n*4), dtype=np.uint32) >> 5
    b = np.fromstring(os.urandom(n*4), dtype=np.uint32) >> 6
    return (a * 67108864.0 + b) / 9007199254740992.0


def getRandomPull(randomizerType: int):
    pulled = [0] * NUMBERS_PULLED
    possibleNumbers = [*range(NUMBER_MAX)]

    if randomizerType == 0:
        # Randomizer No.1
        rnNormalized = rand(NUMBERS_PULLED)
        for i in range(NUMBERS_PULLED):
            indexToPop = math.trunc(len(possibleNumbers) * rnNormalized[i])
            pulled[i] = possibleNumbers.pop(indexToPop)
    elif randomizerType == 1:
        # Randomizer No.2
        for i in range(NUMBERS_PULLED):
            indexToPop = random.randrange(0, len(possibleNumbers))
            pulled[i] = possibleNumbers.pop(indexToPop)

    sanityCheck(pulled, 0)
    addToGroups(pulled, 0)

    return pulled


def sanityCheck(row, offset):
    possible = [0] * NUMBER_MAX

    for number in row:
        i = int(number) - offset
        if not (i >= 0 and i < NUMBER_MAX):
            raise Exception("Out of range")

        if (possible[i] > 0):
            raise Exception("Same Number")
        possible[i] += 1


def setNumbers(name):
    '''
    Gets/sets numbers.

    Parameter is CSV file path. CSV data in format [date,numb1,numb2,...].
    When parameter is empty numbers are generated.

    '''

    if name:
        with open(name) as file:
            reader = csv.reader(file, delimiter=';')

            firstRow = True
            for row in reader:
                # Skips header
                if (firstRow):
                    firstRow = False
                    continue

                extractedData = row[5:10]
                addToGroups(extractedData, 1)
                sanityCheck(extractedData, 1)

                for j, numb in enumerate(extractedData):
                    numbers[int(numb)-1] += 1
    else:
        for _ in range(0, TO_GENERATE):
            randomRow = getRandomPull(0)
            for j, numb in enumerate(randomRow):
                numbers[int(numb)-1] += 1


def getAllPossible():
    # Create a list of all elements to choose from
    elements = list(range(1, NUMBER_MAX + 1))
    # Generate all combinations of k elements without repetition
    return list(combinations(elements, NUMBERS_GUESSED))


# Process data
setNumbers("eurojackpot.csv")

pairTuples = []
for i, row in enumerate(pairs):
    for j, number in enumerate(row):
        if (i < j):
            key = set([i+1, j+1])
            pairTuples.append((key, number))
sortedPairs = sorted(pairTuples, key=lambda item: item[1])
TO_SHOW = 100  # len(sortedPairs)
print(sortedPairs[0:TO_SHOW])

tripletsTuples = []
updated = [[[0.0]*NUMBER_MAX for i in range(NUMBER_MAX)]
           for j in range(NUMBER_MAX)]
for i, area in enumerate(triplets):
    for j, row in enumerate(area):
        for k, number in enumerate(row):
            # to remove unnecessary data
            if i >= j or j >= k or i >= k:
                continue
            # to show specific num of occurrence
            if number <= 2:
                updated[i][j][k] = 1 / (number*number*number + 1)

            key = set([i+1, j+1, k+1])
            tripletsTuples.append((key, number))
sortedTriplets = sorted(tripletsTuples, key=lambda item: item[1])


def display():
    '''
    Display data.
    '''
    # Display data
    # Pairs
    figPairs = px.imshow(pairs, origin='lower')
    figPairs.update_layout(scene=dict(
        xaxis_title='Order',
        yaxis_title='Number',
    ))
    figPairs.show()

    # Triplets
    figTriplet = go.Figure()
    figTriplet.add_trace(go.Scatter3d(name='Triplets',
                                      x=np.array(gridX, np.int32).flat, y=np.array(gridY, np.int32).flat, z=np.array(gridZ, np.int32).flat, mode='markers', marker=dict(
                                          color=np.array(updated).flat,
                                          colorscale=[[0, 'rgba(0,0,0,0)'], [
                                              1, 'rgba(180,245,128,255)']],
                                          size=2
                                      ), text=np.array(updated).flat,))
    figTriplet.show()


# Create list containing only triplets, that meet threshold.
THRESHOLD = 0
tripletList = []
for tripletTuple in sortedTriplets:
    if (tripletTuple[1] <= THRESHOLD):
        tripletList.append((tripletTuple[0], tripletTuple[1]))
    else:
        break

# Count number that is the most occurring in triplets that meet threshold
countList = [0] * NUMBER_MAX
for tripletTuple in tripletList:
    for numberID in range(NUMBER_MAX):
        triplet = list(tripletTuple[0])
        for i in range(0, 3):
            if (int(triplet[i]) == numberID+1):
                countList[numberID] = countList[numberID] + 1

countTuple = [(j, "Number ID %d" % (i+1)) for i, j in enumerate(countList)]
print(sorted(countTuple, reverse=True))

gridX = [[[j]*NUMBER_MAX for i in range(NUMBER_MAX)]
         for j in range(NUMBER_MAX)]
gridY = [[[i]*NUMBER_MAX for i in range(NUMBER_MAX)]
         for j in range(NUMBER_MAX)]
gridZ = [[k for k in range(NUMBER_MAX)] for i in range(NUMBER_MAX)]*NUMBER_MAX


def evaluateCombinationPrev(combination):
    # remove set() in pairs and triplets
    sumNumb = 0
    sumPair = 0
    sumTriple = 0
    for number in combination:
        sumNumb += numbers[number-1]

    for pairTuple in pairTuples:
        if all(digit in combination for digit in pairTuple[0]):
            sumPair += pairTuple[1]

    for tripletsTuple in tripletsTuples:
        if all(digit in combination for digit in tripletsTuple[0]):
            sumTriple += tripletsTuple[1]
    return (combination, sumNumb, sumPair, sumTriple)


def evaluateCombination(combination):
    sumNumb = 0
    sumPair = 0
    sumTriple = 0
    for number in combination:
        sumNumb += numbers[number-1]

    for pairTuple in pairTuples:
        if combination & pairTuple[0]:
            sumPair += pairTuple[1]

    for tripletsTuple in tripletsTuples:
        if combination & tripletsTuple[0]:
            sumTriple += tripletsTuple[1]
    return (combination, sumNumb, sumPair, sumTriple)


results = []


def collect_result(result):
    global results
    results.append(result)


def parallelProcess(save):
    start_time = time.time()
    allPossibleTuples = getAllPossible()
    allPossible = [set(item) for item in allPossibleTuples]

    results = [None] * len(allPossible)
    mid_time = time.time()
    print('Combinations generated in:', mid_time - start_time, 'seconds')
    # protect the entry point
    if __name__ == '__main__':
        # create and configure the process pool
        pool = Pool()
        # issue tasks to the process pool
        for combination in allPossible:
            pool.apply_async(evaluateCombination, args=(
                combination,), callback=collect_result)
        # close the process pool
        pool.close()
        # wait for all tasks to finish
        pool.join()

        if save:
            with open('raw/data_file.pkl', 'wb') as file:
                pickle.dump(results, file)

    end_time = time.time()
    print('Parallel process done in:', end_time - mid_time, 'seconds')


parallelProcess(False)

start_time = time.time()
check.processData(results, 'euro_')
end_time = time.time()
print('Done in:', end_time - start_time, 'seconds')
