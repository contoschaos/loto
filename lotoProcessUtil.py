import numpy as np
import csv
import math
import random
import plotly.express as px
import plotly.graph_objects as go

import lotoStaticUtil as st

# Index/position is specifying the number (number[0] means number 1, number[26] means 27 ...)
# Count of times each Number was drawn
numbers = [0] * st.NUMBER_MAX

# Dimension are next numbers
# Count of times each number pair was drawn, orderless
pairs = [
    [0]*st.NUMBER_MAX for _ in range(st.NUMBER_MAX+1)]
# Count of times each number triplet was drawn, orderless
triplets = [[[0]*st.NUMBER_MAX for _ in range(st.NUMBER_MAX)]
            for _ in range(st.NUMBER_MAX)]


def addToGroups(pulled, offset):
    copy = np.array(pulled).tolist()  # all pulled
    # Number loop
    for _ in range(st.NUMBERS_PULLED):
        x = int(copy.pop(0))
        xi = int(x) - offset
        secondCp = np.array(copy).tolist()
        # Pair loop
        for y in copy:
            yi = int(y) - offset

            sortedKeys = sorted([xi, yi])

            # removes  order for pairs
            pairs[sortedKeys[0]][sortedKeys[1]] += 1

            # Each pair is part of triplet
            secondCp.pop(0)  # Copy without Y
            # Triplet loop
            for z in secondCp:
                zi = int(z) - offset
                # Removes  order for triplets
                sortedKeys = sorted([xi, yi, zi])
                triplets[sortedKeys[0]][sortedKeys[1]][sortedKeys[2]] += 1


def getRandomPull(randomizerType: int):
    pulled = [0] * st.NUMBERS_PULLED
    possibleNumbers = [*range(st.NUMBER_MAX)]

    if randomizerType == 0:
        # Randomizer No.1
        rnNormalized = st.rand(st.NUMBERS_PULLED)
        for i in range(st.NUMBERS_PULLED):
            indexToPop = math.trunc(len(possibleNumbers) * rnNormalized[i])
            pulled[i] = possibleNumbers.pop(indexToPop)
    elif randomizerType == 1:
        # Randomizer No.2
        for i in range(st.NUMBERS_PULLED):
            indexToPop = random.randrange(0, len(possibleNumbers))
            pulled[i] = possibleNumbers.pop(indexToPop)

    sanityCheck(pulled, 0)
    addToGroups(pulled, 0)

    return pulled


def sanityCheck(row, offset):
    possible = [0] * st.NUMBER_MAX

    for number in row:
        i = int(number) - offset
        if not (i >= 0 and i < st.NUMBER_MAX):
            raise Exception("Out of range")

        if (possible[i] > 0):
            raise Exception("Same Number")
        possible[i] += 1


def setNumbers(name):
    '''
    Gets/sets numbers.

    Parameter is CSV file path. CSV data in format [date,numb1,numb2,...].
    When parameter is empty numbers are generated.

    '''

    if name:
        with open(name) as file:
            reader = csv.reader(file, delimiter=';')

            firstRow = True
            prevDate = ''
            prevExtractedData = []
            for row in reader:
                # Skips header
                if (firstRow):
                    firstRow = False
                    continue

                extractedData = row[3:10]

                # Skips duplicates
                if (prevDate == row[1]):
                    if (np.array_equal(extractedData, prevExtractedData)):
                        print("Skipped duplicate:{}".format(prevDate))
                        continue

                prevDate = row[1]
                prevExtractedData = extractedData

                addToGroups(extractedData, 1)
                sanityCheck(extractedData, 1)

                for j, numb in enumerate(extractedData):
                    numbers[int(numb)-1] += 1
    else:
        for _ in range(0, st.TO_GENERATE):
            randomRow = getRandomPull(0)
            for j, numb in enumerate(randomRow):
                numbers[int(numb)-1] += 1


def display(updated, gridX, gridY, gridZ):
    '''
    Display data.
    '''
    # Display data
    # Pairs
    figPairs = px.imshow(pairs, origin='lower')
    figPairs.update_layout(scene=dict(
        xaxis_title='Order',
        yaxis_title='Number',
    ))
    figPairs.show()

    # Triplets
    figTriplet = go.Figure()
    figTriplet.add_trace(go.Scatter3d(name='Triplets',
                                      x=np.array(gridX, np.int32).flat, y=np.array(gridY, np.int32).flat, z=np.array(gridZ, np.int32).flat, mode='markers', marker=dict(
                                          color=np.array(updated).flat,
                                          colorscale=[[0, 'rgba(0,0,0,0)'], [
                                              1, 'rgba(180,245,128,255)']],
                                          size=2
                                      ), text=np.array(updated).flat,))
    figTriplet.show()
